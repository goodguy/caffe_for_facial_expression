project( Examples )

FIND_PACKAGE( OpenCV REQUIRED )


file(GLOB_RECURSE EXAMPLES_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)

if(OpenCV_FOUND)
foreach(source ${EXAMPLES_SOURCES})
    include_directories(${OpenCV_INCLUDE_DIR})
    #    get file name
    get_filename_component(name ${source} NAME_WE)  
    
    #get folder name
    get_filename_component(path ${source} PATH)      
    get_filename_component(folder ${path} NAME_WE)      
    
    add_executable(${name} ${source})
    target_link_libraries(${name} caffe ${OpenCV_LIBS})
    set_target_properties(${name} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${folder})
endforeach(source)
endif()
