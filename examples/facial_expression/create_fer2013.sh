#!/usr/bin/env sh
# This script converts the mnist data into lmdb/leveldb format,
# depending on the value assigned to $BACKEND.

EXAMPLE=examples/facial_expression
DATA=data/fer2013
BUILD=build/examples/facial_expression

BACKEND="lmdb"

echo "Creating ${BACKEND}..."

rm -rf $EXAMPLE/fer2013_train_${BACKEND}
rm -rf $EXAMPLE/fer2013_test_${BACKEND}

$BUILD/convert_fer2013_data $DATA/train \
    $DATA/train_labels.txt $EXAMPLE/fer2013_train_${BACKEND} 
$BUILD/convert_fer2013_data $DATA/test \
    $DATA/test_labels.txt $EXAMPLE/fer2013_test_${BACKEND} 

echo "Done."
