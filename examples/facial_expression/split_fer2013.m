clear all;
clc;
close all;
load('fer2013.mat');

file_size = size(fer2013,1);

images = zeros(file_size, 48*48);
labels = zeros(file_size, 1);

for(k = 1:file_size-1)
    images(k,:) = str2num(fer2013{k+1, 2});
    labels(k,:) = fer2013{k+1, 1};
end


images = uint8(images);
labels = uint8(labels);



 
 
 fid = fopen('fer2013/train_labels.txt', 'wt');
 for k = 1:28709
 	
   image = images(k,:);
   image = reshape(image, [48,48]);
   imwrite(image', strcat('fer2013/train/',int2str(k),'.png'))
    
    fprintf(fid,'%u\n',labels(k,1));
 end     
 fclose(fid);
 
  
 fid = fopen('fer2013/test_labels.txt', 'wt');
 for k =  28710:(28710+3588)
 	
   image = images(k,:);
   image = reshape(image, [48,48]);
   imwrite(image', strcat('fer2013/test/',int2str(k-28709),'.png'))
    
    fprintf(fid,'%u\n',labels(k,1));
 end     
 fclose(fid);