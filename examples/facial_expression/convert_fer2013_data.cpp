#include <gflags/gflags.h>
#include <glog/logging.h>
#include <google/protobuf/text_format.h>
#include <lmdb.h>
#include <stdint.h>
#include <sys/stat.h>


#include <opencv2/opencv.hpp>

#include <fstream>  // NOLINT(readability/streams)
#include <string>

#include "caffe/proto/caffe.pb.h"

using namespace caffe;  // NOLINT(build/namespaces)
using std::string;

//DEFINE_string(backend, "lmdb", "The backend for storing the result");

void convert_dataset(const char* image_path, const char* label_filename,
        const char* db_path, const string& db_backend) {
  // Open files

  std::ifstream label_file(label_filename, std::ios::in);
  CHECK(label_file) << "Unable to open file " << label_filename;

  uint32_t num_items;
  uint32_t num_labels;
  uint32_t rows;
  uint32_t cols;

  cv::VideoCapture cap_for_image;
  cap_for_image.open(std::string(image_path) + std::string("/%1d.png") );
  num_items = (std::size_t)cap_for_image.get(CV_CAP_PROP_FRAME_COUNT);
  if(!cap_for_image.isOpened()) {
      std::cout << "Open files is failed " << std::endl;
      return ;
  }
  std::cout << "Max: " << num_items << std::endl;

  num_labels = num_items;

  rows = 48;
  cols = 48;

  // lmdb
  MDB_env *mdb_env;
  MDB_dbi mdb_dbi;
  MDB_val mdb_key, mdb_data;
  MDB_txn *mdb_txn;

  // Open db
  if (db_backend == "lmdb") {  // lmdb
    CHECK_EQ(mkdir(db_path, 0744), 0)
        << "mkdir " << db_path << "failed";
    CHECK_EQ(mdb_env_create(&mdb_env), MDB_SUCCESS) << "mdb_env_create failed";
    CHECK_EQ(mdb_env_set_mapsize(mdb_env, 1099511627776), MDB_SUCCESS)  // 1TB
        << "mdb_env_set_mapsize failed";
    CHECK_EQ(mdb_env_open(mdb_env, db_path, 0, 0664), MDB_SUCCESS)
        << "mdb_env_open failed";
    CHECK_EQ(mdb_txn_begin(mdb_env, NULL, 0, &mdb_txn), MDB_SUCCESS)
        << "mdb_txn_begin failed";
    CHECK_EQ(mdb_open(mdb_txn, NULL, 0, &mdb_dbi), MDB_SUCCESS)
        << "mdb_open failed. Does the lmdb already exist? ";
  } 
  // Storing to db
  char label;
  char* pixels = new char[rows * cols];
  int count = 0;
  const int kMaxKeyLength = 10;
  char key_cstr[kMaxKeyLength];
  string value;

  Datum datum;
  datum.set_channels(1);
  datum.set_height(rows);
  datum.set_width(cols);
  for (int item_id = 0; item_id < num_items; ++item_id) {
    cv::Mat image;

    cap_for_image.grab();

    cap_for_image.retrieve(image, 0); 

    CHECK_EQ(image.rows, rows);
    CHECK_EQ(image.cols, cols);

    cv::cvtColor( image, image, CV_BGR2GRAY );

    cv::Scalar mean;
    cv::Scalar stddev;

    cv::meanStdDev(image, mean, stddev);

    cv::Mat image2;
    cv::equalizeHist(image, image2);

    for(int i = 0; i < image.rows; ++i){
        for(int j = 0; j < image.cols; ++j){
            pixels[image.cols*i + j] = image2.at<unsigned char>(i,j);
        }
    }



    int label_int;
    label_file >> label_int;
    label = (char)label_int;

    if(label == 1)  continue;
    else if(label > 1){
        label = label - 1;
    }
    //cv::imshow("image", image);
    cv::imshow("image2", image2);


    //std::cout << (int)label << std::endl;
    cv::waitKey(1);
    datum.set_data(pixels, rows*cols);
    datum.set_label(label);
    snprintf(key_cstr, kMaxKeyLength, "%08d", item_id);
    datum.SerializeToString(&value);
    string keystr(key_cstr);

    // Put in db
    if (db_backend == "lmdb") {  // lmdb
      mdb_data.mv_size = value.size();
      mdb_data.mv_data = reinterpret_cast<void*>(&value[0]);
      mdb_key.mv_size = keystr.size();
      mdb_key.mv_data = reinterpret_cast<void*>(&keystr[0]);
      CHECK_EQ(mdb_put(mdb_txn, mdb_dbi, &mdb_key, &mdb_data, 0), MDB_SUCCESS)
          << "mdb_put failed";
    } 
    if (++count % 1000 == 0) {
        // Commit txn
        if (db_backend == "lmdb") {  // lmdb
            CHECK_EQ(mdb_txn_commit(mdb_txn), MDB_SUCCESS)
                << "mdb_txn_commit failed";
            CHECK_EQ(mdb_txn_begin(mdb_env, NULL, 0, &mdb_txn), MDB_SUCCESS)
                << "mdb_txn_begin failed";
        }  
    }
  }
  // write the last batch
  if (count % 1000 != 0) {
      if (db_backend == "lmdb") {  // lmdb
          CHECK_EQ(mdb_txn_commit(mdb_txn), MDB_SUCCESS) << "mdb_txn_commit failed";
          mdb_close(mdb_env, mdb_dbi);
          mdb_env_close(mdb_env);
      }  
  }
  delete pixels;
}

int main(int argc, char** argv) {

    const string& db_backend("lmdb");
    convert_dataset(argv[1], argv[2], argv[3], db_backend);

  return 0;
}
